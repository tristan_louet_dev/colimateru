#include "Transform.h"
#include <cmath>
#include <cassert>

Transform::Transform() :
	mat(0.f, 16)
{}

Transform::Transform(const Transform& other) :
	mat(other.mat)
{}

Transform::Transform(std::initializer_list<double> il) :
	mat{il}
{
	assert(il.size() == 16);
}

Transform::Transform(Transform&& other) :
	mat{std::move(other.mat)}
{}

Transform& Transform::operator=(Transform&& other)
{
	mat = std::move(other.mat);

	return *this;
}

Transform& Transform::operator=(Transform& other)
{
	mat = other.mat;

	return *this;
}

double& Transform::operator()(size_t row, size_t column)
{
	return mat[row*4+column];
}

Transform& Transform::operator*=(const Transform& other)
{
	*this = *this * other;

	return *this;
}

Vec3 operator*(const Transform& t, const Vec3& v)
{
	return {
		t.mat[0]*v.x + t.mat[1]*v.y + t.mat[2]*v.z + t.mat[3],
		t.mat[4]*v.x + t.mat[5]*v.y + t.mat[6]*v.z + t.mat[7],
		t.mat[8]*v.x + t.mat[9]*v.y + t.mat[10]*v.z + t.mat[11]
	};
}

Transform operator*(const Transform& t1, const Transform& t2)
{
	// Avec boucle

	Transform result;

	for(size_t row = 0; row < 4; ++row)
	{
		for(size_t col = 0; col < 4; ++col)
		{
			for(size_t i = 0; i < 4; ++i)
			{
				result.mat[row*4+col] += t1.mat[row*4+i] * t2.mat[i*4+col];
			}
		}
	}

	return result;

	/* Sans boucle (pas fini)

	return {
		// first row
		t1.mat[0]*t2.mat[0] + t1.mat[1]*t2.mat[4] + t1.mat[2]*t2.mat[8] + t1.mat[3]*t2.mat[12],
		t1.mat[0]*t2.mat[1] + t1.mat[1]*t2.mat[5] + t1.mat[2]*t2.mat[9] + t1.mat[3]*t2.mat[13],
		t1.mat[0]*t2.mat[2] + t1.mat[1]*t2.mat[6] + t1.mat[2]*t2.mat[10] + t1.mat[3]*t2.mat[14],
		t1.mat[0]*t2.mat[3] + t1.mat[1]*t2.mat[7] + t1.mat[2]*t2.mat[11] + t1.mat[3]*t2.mat[15],

		// second row
		t1.mat[0]*t2.mat[0] + t1.mat[1]*t2.mat[4] + t1.mat[2]*t2.mat[8] + t1.mat[3]*t2.mat[12],
		t1.mat[0]*t2.mat[1] + t1.mat[1]*t2.mat[5] + t1.mat[2]*t2.mat[9] + t1.mat[3]*t2.mat[13],
		t1.mat[0]*t2.mat[2] + t1.mat[1]*t2.mat[6] + t1.mat[2]*t2.mat[10] + t1.mat[3]*t2.mat[14],
		t1.mat[0]*t2.mat[3] + t1.mat[1]*t2.mat[7] + t1.mat[2]*t2.mat[11] + t1.mat[3]*t2.mat[15],

	};*/
}

Transform translation(const Vec3& trans)
{
	return {
		1, 0, 0, trans.x,
		0, 1, 0, trans.y,
		0, 0, 1, trans.z,
		0, 0, 0, 1
	};
}

Transform rotation(double rad, Axis axis)
{
	using std::cos;
	using std::sin;

	switch(axis)
	{
		case Axis::X:
			return {
				1, 0, 0, 0,
				0, cos(rad), -sin(rad), 0,
				0, sin(rad), cos(rad), 0,
				0, 0, 0, 1
			};

		case Axis::Y:
			return {
				cos(rad), 0, sin(rad), 0,
				0, 1, 0, 0,
				-sin(rad), 0, cos(rad), 0,
				0, 0, 0, 1
			};

		case Axis::Z:
			return {
				cos(rad), -sin(rad), 0, 0,
				sin(rad), cos(rad), 0, 0,
				0, 0, 1, 0,
				0, 0, 0, 1
			};

		default:
			assert(false);
			return {};
	}
}

Transform scale(const Vec3& s)
{
	return {
		s.x, 0, 0, 0,
		0, s.y, 0, 0,
		0, 0, s.z, 0,
		0, 0, 0, 1
	};
}
