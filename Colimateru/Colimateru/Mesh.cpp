#include "Mesh.h"

#include <cassert>
#include <iostream>
#include <string>
#include <algorithm>
#include <cmath>
#include <limits>

Mesh::Mesh(Mesh&& other) :
	vertices{std::move(other.vertices)},
	normals{std::move(other.normals)},
	links{std::move(other.links)},
	indices{std::move(other.indices)}
{}

Mesh::Mesh(const std::string& filename)
{
	std::ifstream file{filename};

	file >> *this;
}

Mesh& Mesh::operator=(Mesh&& other)
{
	if(&other != this)
	{
		vertices = std::move(other.vertices);
		normals = std::move(other.normals);
		links = std::move(other.links);
		indices = std::move(other.indices);
	}

	return *this;
}

Mesh& Mesh::operator=(const Mesh& other)
{
	if(&other != this)
	{
		vertices = other.vertices;
		normals = other.normals;
		links = other.links;
		indices = other.indices;
	}

	return *this;
}

Mesh& Mesh::operator+=(const Mesh& other)
{
	auto offset_v = vertices.size();
	auto offset_n = normals.size();
	auto offset_l = links.size();
	auto offset_i = indices.size();

	vertices.insert(std::end(vertices), std::begin(other.vertices), std::end(other.vertices));
	normals.insert(std::end(normals), std::begin(other.normals), std::end(other.normals));
	links.insert(std::end(links), std::begin(other.links), std::end(other.links));
	indices.insert(std::end(indices), std::begin(other.indices), std::end(other.indices));

	std::for_each(std::begin(links) + offset_l, std::end(links),
		[&] (std::pair<std::size_t, std::size_t>& i)
		{
			i.first += offset_v;
			i.second += offset_n;
		});

	std::for_each(std::begin(indices) + offset_i, std::end(indices),
		[&] (std::size_t& i)
		{
			i += offset_v;
		});

	return *this;
}

void Mesh::fromFile(const std::string& filename)
{
	std::ifstream file{filename};

	file >> *this;
}

void Mesh::clear()
{
	vertices.clear();
	normals.clear();
	links.clear();
	indices.clear();
}

void Mesh::apply(const Transform& t)
{
	const auto mul = 
		[&] (Vec3& v)
		{
			v = t*v;
		};

	std::for_each(std::begin(vertices), std::end(vertices), mul);
	std::for_each(std::begin(normals), std::end(normals), mul);
}

void Mesh::link(std::size_t vertex, std::size_t normal)
{
	links.emplace_back(vertex, normal);
}

void Mesh::addVertex(const Vec3& v)
{
	vertices.push_back(v);
}

void Mesh::addVertex(double x, double y, double z)
{
	vertices.emplace_back(x, y, z);
}

void Mesh::addNormal(const Vec3& n)
{
	normals.push_back(n);
}

void Mesh::addNormal(double x, double y, double z)
{
	normals.emplace_back(x, y, z);
}

void Mesh::addFace(std::size_t a, std::size_t b, std::size_t c)
{
	indices.push_back(a);
	indices.push_back(b);
	indices.push_back(c);
}

void Mesh::computeNormals()
{
	normals.clear();
	normals.resize(vertices.size());

	unsigned int face = 0;

	while(face < indices.size())
	{
		const auto i = indices[face], ii = indices[face + 1], iii = indices[face + 2];
		const auto& A = vertices[i];
		const auto& B = vertices[ii];
		const auto& C = vertices[iii];

		auto N = (B-A) ^ (C-A);

		normals[i] += N;
		normals[ii] += N;
		normals[iii] += N;

		face += 3;
	}

	for(auto& n : normals)
		n.normalize();
}

std::vector<Vec3>& Mesh::getVertices()
{
	return vertices;
}
void Mesh::twist(double omega, Axis axis)
{
	using std::cos;
	using std::sin;

	for(std::size_t i = 0; i < vertices.size(); ++i)
	{
		auto& v = vertices[i];
		Transform J;
		double alpha, cosa, sina;

		switch(axis)
		{

			case Axis::X:
			{
				alpha = omega * v.x;
				cosa = cos(alpha);
				sina = sin(alpha);

				J = {
					1, -sina*v.z + cosa*v.y, cosa*v.z + sina*v.y, 0,
					v.x, -sina*v.z + cosa, cosa*v.z + sina, 0,
					v.x, -sina + cosa*v.y, cosa + sina*v.y, 0,
					0, 0, 0, 1
				};

				auto z = cosa*v.z + sina*v.y;
				auto y = -sina*v.z + cosa*v.y;
				v.z = z;
				v.y = y;
			}
			break;

			case Axis::Y:
			{
				alpha = omega * v.y;
				cosa = cos(alpha);
				sina = sin(alpha);

				J = {
					cosa + sina*v.z, v.y, -sina + cosa*v.x, 0,
					cosa*v.x + sina*v.z, 1, -sina*v.x + cosa*v.z, 0,
					cosa*v.x + sina, v.y, -sina*v.x + cosa, 0,
					0, 0, 0, 1
				};

				auto x = cosa*v.x + sina*v.z;
				auto z = -sina*v.x + cosa*v.z;
				v.z = z;
				v.x = x;
			}
			break;

			case Axis::Z:
			{
				alpha = omega * v.z;
				cosa = cos(alpha);
				sina = sin(alpha);

				J = {
					cosa + sina*v.y, -sina + cosa*v.y, v.z, 0,
					cosa*v.x + sina, -sina*v.x + cosa, v.z, 0,
					cosa*v.x + sina*v.y, -sina*v.x + cosa*v.y, 1, 0,
					0, 0, 0, 1
				};

				auto x = cosa*v.x + sina*v.y;
				auto y = -sina*v.x + cosa*v.y;
				v.x = x;
				v.y = y;
			}
			break;

			default:
			break;
		}

		// on va chercher toutes les normales
		for(auto& link : links)
		{
			// liées à ce sommet
			if(link.first == i+1)
			{
				auto& n = normals[link.second -1]; // on oublie pas le -1 ...
				link.second = normals.size(); // on change le lien vertex/normal
				addNormal(J*n); // avec la nouvelle normale
			}
		}
	}
}

void Mesh::taper(double sa, double sb, double za, double zb)
{
	Transform J;
	int i = 0;

	double detA = 2 * (za - zb) * (sa - sb);
	double detB = -3 * (za*za - zb*zb) * (sa - sb);
	double detC = 6 * (za*za*zb - zb*zb*za) * (sa - sb);
	double detD = sb*(-pow(za, 4) - 3 * za*za*zb*zb + 4 * pow(za, 3)*zb) - sa*(pow(zb, 4) + 3 * za*za*zb*zb - 4 * za*pow(zb, 3));
	double detM = -pow(za - zb, 4);
	double A = detA / detM;
	double B = detB / detM;
	double C = detC / detM;
	double D = detD / detM;

	for (auto& v : vertices)
	{
		double sz;
		if (v.z < za)
		{
			sz = sa;

			J = {
				sa, sa*v.y, v.z, 0,
				sa*v.x, sa, v.z, 0,
				v.z, sa*v.y, 1, 0,
				0, 0, 0, 1
			};
		}
		else if (v.z > zb)
		{
			sz = sb;

			J = {
				sb, sb*v.y, v.z, 0,
				sb*v.x, sb, v.z, 0,
				v.z, sb*v.y, 1, 0,
				0, 0, 0, 1
			};
		}
		else
		{
			sz = A*pow(v.z, 3) + B*v.z*v.z + C*v.z + D;

			J = {
				sz, sz*v.y, v.z, 0,
				sz*v.x, sz, v.z, 0,
				sz*v.x, sz*v.y, 1, 0,
				0, 0, 0, 1
			};
		}

		v.x *= sz;
		v.y *= sz;

		for(auto& link : links)
		{
			// liées à ce sommet
			if(link.first == i+1)
			{
				auto& n = normals[link.second -1]; // on oublie pas le -1 ...
				link.second = normals.size(); // on change le lien vertex/normal
				addNormal(J*n); // avec la nouvelle normale
			}
		}

		++i;
	}
}

std::ostream& operator<<(std::ostream& os, const Mesh& m)
{
	assert(m.links.size() % 3 == 0 && "links non divisible par 3");

	for(const auto& v : m.vertices)
	{
		os << "v " << v << '\n';
	}
	os << '\n';

	for(const auto& vn : m.normals)
	{
		os << "vn " << vn << '\n';
	}
	os << '\n';

	for(std::size_t i = 0; i < m.links.size();)
	{
		os 	<< "f " << m.links[i].first << "//" << m.links[i].second << ' ';
		++i;
		os			<< m.links[i].first << "//" << m.links[i].second << ' ';
		++i;
		os			<< m.links[i].first << "//" << m.links[i].second << '\n';
		++i;
	}

	return os;
}

std::istream& operator>>(std::istream& is, Mesh& m)
{
	std::string type, line;
	double x, y, z;
	int a, b, c;

	m.clear();

	is >> type;

	if(type == "OFF")
	{
		int nVertices, nFaces, nEdges;
		double max{ -std::numeric_limits<double>::infinity() };
		Vec3 sum;
		is >> nVertices >> nFaces >> nEdges;

		m.vertices.reserve(nVertices);
		m.indices.reserve(nFaces);

		for(int i = 0; i < nVertices; ++i)
		{
			is >> x >> y >> z;
			m.addVertex(x, y, z);

			// used for normalization
			if(x > max)
				max = x;
			if(y > max)
				max = y;
			if(z > max)
				max = z;

			// used to center
			sum.x += x; sum.y += y; sum.z += z;
		}

		for(int i = 0; i < nFaces; ++i)
		{
			is >> a >> a >> b >> c;
			m.addFace(a, b, c);
		}
		sum /= nVertices;
		m.apply(translation(-sum) * scale(Vec3{ 1.0 / max }));
	}
	else // OBJ format
	{
		std::string line;
		char sep;

		while(!is.eof())
		{
			is >> type;

			if(type == "v")
			{
				is >> x >> y >> z;
				m.addVertex({ x, y, z });
			}
			else if(type == "vn")
			{
				is >> x >> y >> z;
				m.addNormal({ x, y, z });
			}
			else if(type == "f")
			{
				type = ""; // reset à chaine vide pour eviter les erreurs

				is >> a >> sep >> sep >> b;
				m.link(a, b);

				is >> a >> sep >> sep >> b;
				m.link(a, b);

				is >> a >> sep >> sep >> b;
				m.link(a, b);
			}
			else
			{
				std::getline(is, line);
			}
		}
	}

	return is;
}

Mesh operator+(const Mesh& m1, const Mesh& m2)
{
	Mesh result{m1};

	result += m2;

	return result;
}