#pragma once

#include "Vec3.h"
#include <initializer_list>
#include <valarray>
#include <cmath>

#ifdef _MSC_VER
#	define constexpr const
#endif

constexpr double pi = std::atan(1)*4;

enum class Axis
{
	X,
	Y,
	Z
};

class Transform
{

public:

	Transform();

	Transform(std::initializer_list<double> il);

	Transform(const Transform&);

	Transform(Transform&& other);

	Transform& operator=(Transform&& other);

	Transform& operator=(Transform& other);


	double& operator()(size_t row, size_t column);

	Transform& operator*=(const Transform& other);

	friend Transform operator*(const Transform& t1, const Transform& t2);

	std::valarray<double> mat;
};

Vec3 operator*(const Transform& t, const Vec3& v);

Transform translation(const Vec3& trans);

Transform rotation(double rad, Axis axis);

Transform scale(const Vec3& s);