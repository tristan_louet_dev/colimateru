#pragma once

#include "Transform.h"
#include <vector>
#include <ostream>
#include <fstream>

class Mesh
{

public:

	// Default constructor
	Mesh() = default;

	// Copy constructor
	Mesh(const Mesh& other) = default;

	// Move constructor
	Mesh(Mesh&& other);

	Mesh(const std::string& filename);

	~Mesh() = default;

	Mesh& operator=(Mesh&& other);
	Mesh& operator=(const Mesh& other);

	Mesh& operator+=(const Mesh& other);

	void fromFile(const std::string& filename);

	void clear();

	void link(std::size_t vertex, std::size_t normal); 
	
	void addVertex(const Vec3& v); 
	void addVertex(double x, double y, double z);

	void addFace(std::size_t a, std::size_t b, std::size_t c);
	
	void addNormal(const Vec3& n);
	void addNormal(double x, double y, double z);

	void computeNormals();

	std::vector<Vec3>& getVertices();

	// Deformations

	void twist(double omega, Axis axis);
	void taper(double sa, double sb, double za, double zb);
	
	friend std::ostream& operator<<(std::ostream& os, const Mesh& m);
	friend std::istream& operator>>(std::istream& is, Mesh& m);

	void apply(const Transform& t);

private:

	std::vector<Vec3> vertices;
	std::vector<std::size_t> indices;

	std::vector<Vec3> normals;
	std::vector<std::pair<std::size_t, std::size_t>> links;
};

Mesh operator+(const Mesh& m1, const Mesh& m2);