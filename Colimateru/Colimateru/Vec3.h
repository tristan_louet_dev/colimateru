#pragma once

#include <ostream>
#include <vector>

class Vec3
{

public:

	// Basic constructor
	

	Vec3(double a, double b, double c) :
		x{ a }, y{ b }, z{ c }
	{}

	Vec3(double v) :
		x{ v }, y{ v }, z{ v }
	{}

	// Default constructor
	Vec3() = default;

	// Copy constructor
	Vec3(const Vec3&) = default;

	~Vec3() = default;

	void normalize();

	Vec3& operator=(const Vec3& other);

	Vec3& operator+=(const Vec3& other);

	Vec3& operator-=(const Vec3& other);

	Vec3& operator^=(const Vec3& other);

	Vec3& operator*=(double val);

	Vec3& operator/=(double val);

	friend std::ostream& operator<<(std::ostream& os, const Vec3& v);


	double x{0}, y{0}, z{0};
};

Vec3 operator+(const Vec3& v, const Vec3& w);

Vec3 operator-(const Vec3& v, const Vec3& w);
Vec3 operator-(const Vec3& v);

Vec3 operator^(const Vec3& v, const Vec3& w);

Vec3 operator*(const Vec3& v, double val);
Vec3 operator*(double val, const Vec3& v);

Vec3 operator/(const Vec3& v, double val);

double lenght(const Vec3& v);

double dot(const Vec3& v, const Vec3& w);

Vec3 norm(const Vec3& v);

Vec3 lerp(const Vec3& a, const Vec3& b, double u);

std::vector<Vec3> makeFrame(const Vec3& A, const Vec3& B);
