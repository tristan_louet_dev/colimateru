#include "Vec3.h"
#include <cmath>

void Vec3::normalize()
{
	auto l = std::sqrt(x*x + y*y + z*z);
	x /= l;
	y /= l;
	z /= l;
}

Vec3& Vec3::operator=(const Vec3& other)
{
	x = other.x;
	y = other.y;
	z = other.z;

	return *this;
}

Vec3& Vec3::operator+=(const Vec3& other)
{
	x += other.x;
	y += other.y;
	z += other.z;

	return *this;
}

Vec3& Vec3::operator-=(const Vec3& other)
{
	x -= other.x;
	y -= other.y;
	z -= other.z;

	return *this;
}

Vec3& Vec3::operator^=(const Vec3& other)
{
	*this = *this ^ other;

	return *this;
}

Vec3& Vec3::operator*=(double val)
{
	x *= val;
	y *= val;
	z *= val;

	return *this;
}

Vec3& Vec3::operator/=(double val)
{
	x /= val;
	y /= val;
	z /= val;

	return *this;
}

Vec3 operator+(const Vec3& v, const Vec3& w)
{
	return {v.x+w.x, v.y+w.y, v.z+w.z};
}

Vec3 operator-(const Vec3& v, const Vec3& w)
{
	return {v.x-w.x, v.y-w.y, v.z-w.z};
}

Vec3 operator-(const Vec3& v)
{
	return {-v.x, -v.y, -v.z};
}

Vec3 operator^(const Vec3& v, const Vec3& w)
{
	return {
		v.y*w.z - w.y*v.z,
		v.z*w.x - w.z*v.x,
		v.x*w.y - w.x*v.y
	};
}

Vec3 operator*(const Vec3& v, double val)
{
	return {v.x*val, v.y*val, v.z*val};
}

Vec3 operator*(double val, const Vec3& v)
{
	return {v.x*val, v.y*val, v.z*val};
}

Vec3 operator/(const Vec3& v, double val)
{
	return {v.x/val, v.y/val, v.z/val};
}

std::ostream& operator<<(std::ostream& os, const Vec3& v)
{
	os << v.x << ' ' << v.y << ' ' << v.z;

	return os;
}

std::vector<Vec3> makeFrame(const Vec3& A, const Vec3& B)
{
	Vec3 w = B - A;
	Vec3 u{w.y, -w.x, 0};
	Vec3 v = w ^ u;
	
	return {u, v, w};
}

double lenght(const Vec3& v)
{
	return std::sqrt(v.x*v.x + v.y*v.y + v.z*v.z);
}

double dot(const Vec3& v, const Vec3& w)
{
	return v.x*w.x + v.y*w.y + v.z*w.z;
}

Vec3 norm(const Vec3& v)
{
	Vec3 w = v;
	auto l = lenght(v);

	w.x /= l;
	w.y /= l;
	w.z /= l;

	return w;
}
Vec3 lerp(const Vec3& a, const Vec3& b, double u)
{
	return a + Vec3{ b.x - a.x, b.y - a.y, b.z - a.z } *u;
}